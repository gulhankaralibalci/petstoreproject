$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/PetsByStatus.feature");
formatter.feature({
  "name": "Validating number of records with status \u003d available and name \u003d doggie",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Happy path scenario with Swagger service",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@happyPath"
    }
  ]
});
formatter.step({
  "name": "I set the petsByStatus service endpoint for swagger service",
  "keyword": "Given "
});
formatter.match({
  "location": "com.petstoreproject.step_definitions.petsByStatusStepDefinitions.i_set_petsByStatus_service_endpoint_for_swagger_service()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I set a GET request for queryparam status \u003d \"available\"",
  "keyword": "When "
});
formatter.match({
  "location": "com.petstoreproject.step_definitions.petsByStatusStepDefinitions.i_set_a_GET_request_for_queryparam_status(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I calculate how many of result set has name \"doggie\" and validate all results",
  "keyword": "Then "
});
formatter.match({
  "location": "com.petstoreproject.step_definitions.petsByStatusStepDefinitions.i_calculate_how_many_of_result_set_has_name_and_validate_all_results(java.lang.String)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Results are very dynamic! It is expected to be 10 but there are 187 expected:\u003c10\u003e but was:\u003c187\u003e\n\tat org.junit.Assert.fail(Assert.java:89)\n\tat org.junit.Assert.failNotEquals(Assert.java:835)\n\tat org.junit.Assert.assertEquals(Assert.java:647)\n\tat com.petstoreproject.step_definitions.petsByStatusStepDefinitions.i_calculate_how_many_of_result_set_has_name_and_validate_all_results(petsByStatusStepDefinitions.java:49)\n\tat ✽.I calculate how many of result set has name \"doggie\" and validate all results(file:///Users/gulhankarali/IdeaProjects/PetStoreProject/src/test/resources/features/PetsByStatus.feature:7)\n",
  "status": "failed"
});
});