$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/getPetsByStatus.feature");
formatter.feature({
  "name": "",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@happyPath"
    }
  ]
});
formatter.step({
  "name": "I set petsByStatus service endpoint for swagger service",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "I set a GET request for queryparam status \u003d \"available\"",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "I calculate how many of result set has name \"doggie\" and validate results",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});