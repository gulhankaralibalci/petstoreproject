Feature: Validating number of records with status = available and name = doggie

  @happyPath
  Scenario: Happy path scenario with Swagger service
    Given I set the petsByStatus service endpoint for swagger service
    When I set a GET request for queryparam status = "available"
    Then I calculate how many of result set has name "doggie" and validate all results

  @mocking
  Scenario: Happy path scenario with mocking service
    Given I set petsByStatus endpoint for mocking service
    When I set a GET request for queryparam status = "available"
    Then I calculate how many of result set has name "doggie" and validate all results

  @negativeTest
  Scenario: Negative test scenario - 1
    Given I set the petsByStatus service endpoint for swagger service
    When I set a GET request for queryparam status = "availablee"
    Then I receive enum incompatibilty as regards to enum PetStatus

  @negativeTest
  Scenario: Negative test scenario - 2
    Given I set the petsByStatus service endpoint for swagger service
    When I set a GET request for queryparam status = "available"
    Then I calculate how many of result set has name "notadogname" and have zero result

  @negativeTest
  Scenario: Negative test scenario - 3
    Given I set petsByStatus endpoint for mocking service with XML content type
    When I set a GET request for queryparam status = "available"
    Then I validate content types are incompatable



