package com.petstoreproject.pages;


import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.petstoreproject.utilities.ConfigurationReader;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Rule;

public class JavaClient {

    public enum PetStatus {
        available,
        pending,
        sold
    }
    private String uri;
    private boolean mockingOn;
    public static int statusCode;
    public static String contentType;
    public static int count = 0;
    public static String name;
    public static ArrayList<Map<String, String>> myList;
    public static int sizeOfList;
    public static final int SUCCESS = 200;
    public static PetStatus petStatus;
    public static String notCorrectFormat;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule();

    public static Logger logger = Logger.getLogger(JavaClient.class.getName());

    public JavaClient(String uri, boolean mockingOn) {
        this.uri = uri;
        this.mockingOn = mockingOn;
    }

    public boolean isMockingOn() {
        return this.mockingOn;
    }

    public void getPetsByStatus(PetStatus petStatus) throws Exception {
        if (!this.mockingOn) {
            try {
                RestAssured.baseURI = ConfigurationReader.get(this.uri);
                Response response = RestAssured.given().accept(ContentType.JSON).and().
                        queryParam("status", petStatus.toString()).
                        when().get("/pet/findByStatus");

                statusCode = response.getStatusCode();
                contentType = response.contentType();
                if (statusCode != SUCCESS || !contentType.equals("application/json")) {
                    throw new Exception();
                }

                sizeOfList = response.body().path("list.size()");
                myList = (ArrayList)response.body().as(ArrayList.class);

                for(int i = 0; i < sizeOfList; ++i) {
                    if (((String)((Map)myList.get(i)).get("name")).equals(name)) {
                        ++count;
                    }
                }
            } catch (Exception e1) {
                logger.log(Level.INFO, "Swagger server is down or something else occured with status code " + statusCode + ", test will be done with json file on local!");
                JSONParser parser = new JSONParser();
                count = 0;
                int i = 0;
                try {
                    FileReader reader = new FileReader("pets.json");
                    JSONParser jsonParser = new JSONParser();
                    Object object= jsonParser.parse(reader);
                    JSONArray jsonArray= (JSONArray)object;

                    for (int k=0;k<jsonArray.size();k++){
                        JSONObject jsonObject= (JSONObject)jsonArray.get(i);
                        if (jsonObject.get("status").equals(petStatus.toString()) &&
                                jsonObject.get("name").equals(name))
                            count++;
                    }
                } catch (ParseException | IOException e2) {
                    logger.log(Level.INFO, "There is problem with either with parsing or opening json file on local!");
                    throw new Exception("There is problem with either parsing or opening json file on local!");
                }
            }
        } else {
            try {
                RestAssured.baseURI = ConfigurationReader.get(this.uri);

                ResponseBody responseBody = ((Response)RestAssured.given().accept(ContentType.JSON).
                        queryParam("status", petStatus.toString()).
                        get("/pet/findByStatus")).getBody();

                statusCode = ((Response)RestAssured.given().
                        queryParam("status", petStatus.toString()).
                        get("/pet/findByStatus")).getStatusCode();

                contentType = ((Response)RestAssured.given().
                        queryParam("status", petStatus.toString()).
                        get("/pet/findByStatus")).getContentType();

                if (statusCode == 200 && contentType.equals("application/json")) {
                    sizeOfList = (Integer)responseBody.path("list.size()");
                    myList = (ArrayList)responseBody.as(ArrayList.class);

                    for(int i = 0; i < sizeOfList; ++i) {
                        if (((myList.get(i)).get("name")).equals(name)) {
                            ++count;
                        }
                    }
                }
            } catch (Exception e3) {
               e3.printStackTrace();
            }
        }

    }
}
