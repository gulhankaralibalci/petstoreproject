package com.petstoreproject.step_definitions;

import com.petstoreproject.pages.JavaClient;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class petsByStatusStepDefinitions {

    public static JavaClient javaClient;

    @Given("I set the petsByStatus service endpoint for swagger service")
    public void i_set_petsByStatus_service_endpoint_for_swagger_service() {
        javaClient = new JavaClient("swagger.uri", false);
    }

    @Given("I set petsByStatus endpoint for mocking service")
    public void i_set_petByStatus_endpoint_for_mocking_service() throws Exception {
        javaClient = new JavaClient("mocking.uri", true);
    }

    @When("I set a GET request for queryparam status = {string}")
    public void i_set_a_GET_request_for_queryparam_status(String status) {
        switch(status) {
            case "available":
                JavaClient.petStatus = JavaClient.PetStatus.valueOf("available");
                break;
            case "pending":
                JavaClient.petStatus = JavaClient.PetStatus.valueOf("pending");
                break;
            case "sold":
                JavaClient.petStatus = JavaClient.PetStatus.valueOf("sold");
                break;
            default:
                JavaClient.notCorrectFormat = "No enum constant PetStatus." + status;
        }
    }


    @Then("I calculate how many of result set has name {string} and validate all results")
    public void i_calculate_how_many_of_result_set_has_name_and_validate_all_results(String name) throws Exception {
        JavaClient.name = name;
        javaClient.getPetsByStatus(JavaClient.petStatus);
        if (javaClient.isMockingOn()) {
            Assert.assertEquals(200L, (long)JavaClient.statusCode);
            Assert.assertEquals("application/json", JavaClient.contentType);
        }
        Assert.assertEquals("Results are very dynamic! It is expected to be 10 but there are " + JavaClient.count, 10, JavaClient.count);
    }

    @Then("I calculate how many of result set has name {string} and have zero result")
    public void i_calculate_how_many_of_result_set_has_name_and_have_zero_result(String name) throws Exception {
        JavaClient.name = name;
        javaClient.getPetsByStatus(JavaClient.petStatus);
        Assert.assertEquals(0, JavaClient.count);
    }


    @Given("I set petsByStatus endpoint for mocking service with XML content type")
    public void i_set_petsByStatus_endpoint_for_mocking_service_with_XML_content_type() {
        javaClient = new JavaClient("mocking.uri", true);
    }

    @Then("I validate content types are incompatable")
    public void i_validate_content_types_are_incompatable() throws Exception {
        JavaClient.name = "doggie";
        javaClient.getPetsByStatus(JavaClient.petStatus);
        Assert.assertNotEquals("application/json", JavaClient.contentType);
    }

    @Then("I receive enum incompatibilty as regards to enum PetStatus")
    public void I_receive_enum_incompatibilty_as_regards_to_enum_PetStatus() {
        Assert.assertNotNull(JavaClient.notCorrectFormat);
    }

}
